=== Client ===
authmsg-en.def
clientmessages-en.def
defnames.def
kbkorea.def
loadingtipmessages-en.def
map.def

=== Server ===
messages-en.def
plaquemsg-en.def
staticmsg-en.def
storyarcmsg-en.def

=== Shared ===
Proficiencies.def
ProficiencyIds.def
