"eSAccountExist", "The account already exists."
"eSLoginOKLast3Day", "eSLoginOKLast3Day"
"eSAlreadyPlaying", "This user is already playing."
"eSDuplicateSSN", "Duplicate social security number."
"eSInvalidName", "Invalid log-in name."
"eSInvalidAccount", "Invalid account."
"eSSSNNotRegistered", "Social security number not registered."
"eSChangeAccountInfoOK", "eSChangeAccountInfoOK"
"eSLoginOKBuilder", "eSLoginOKBuilder"
"eSBlockInternetBankingFraud", "You a suspect of internet bank fraud."
"eSBlockFirstMoneyTrade", "eSBlockFirstMoneyTrade"
"eSInvalidCountry", "Incalid country."
"eSInvalidPhone1", "Invalid phone number #1."
"default", "No reason specified."
"eSBlockHacking", "You have been blocked for hacking."
"eSKickCharacter", "Your character was kicked."
"eSBlockProgram", "eSBlockProgram"
"eSBlockMobilePhone", "eSBlockMobilePhone"
"eSAllowedPersonalSpecific", "Your paid time is valid."
"eSBannedCharacter", "This character is banned."
"eSEndOfUse", "You have run out of valid play time."
"eSForbiddenIP", "This IP is forbidden."
"eSLoginOKLastDay", "eSLoginOKLastDay"
"eSBlockAccountTrade", "You have been blocked for trading accounts."
"eSLoginOK", "Login was OK."
"eSDisabledIP", "Disabled IP address"
"eSEndOfTrial", "End of trial."
"eSUnknownError", "Unknown Error"
"eSLoginOKLast2Day", "eSLoginOKLast2Day"
"eSBlockMoneyTrade", "You have been blocked for trading money."
"eSBlockRequest", "eSBlockRequest"
"eSCharacterExist", "eSCharacterExist"
"eSCreateCharacterOK", "Character was created OK."
"eSInvalidGender", "Invalid gender."
"eSInvalidEmail", "Invalid email address."
"eSRegistrationRequired", "Please register your social security number"
"eSDoubleLoginSameIP", "Another login is on the same IP."
"eSBlockFalseReport", "You have been blocked for a false report."
"eSEnterWorldOK", "Entered the world OK."
"eSInvalidAddress", "Invalid address."
"eSInvalidPhone2", "Invalid phone number #2."
"eSDuplicateNetsgoUser", "eSDuplicateNetsgoUser"
"eSPasswdDisabled", "Your account has been disabled."
"eSInvalidSSN", "Invalid social security number."
"eSTrialAccount", "eSTrialAccount"
"eSBlockSeriousBugAbuse", "You have been blocked for serious bug abuse."
"eSChangePasswordOK", "The change of password was OK."
"eSIncorrectPasswd", "Incorrect password, old password does not match."
"eSCantChangePassword", "You cannot change your password."
"eSInvalidAttr", "eSInvalidAttr"
"eSPasswdHacking", "Attempted password hacking."
"eSInvalidState", "Invalid State."
"eSBlockWillBeDeleted", "This account is blocked pending deletion."
"eSDisallowedPersonalSpecific", "Your paid time has expired"
"eSBlockTrivialBugAbuse", "You have been blocked for bug abuse."
"eSUnpaidAccount", "The account is overdue."
"eSNewAccountOK", "Everything is OK."
"eSKickAccount", "Your account was kicked."
"eSAllowedIP30Hours", "eSAllowedIPHours"
"eSDeleteCharacterOK", "Deleted the character OK."
"eSIncompleteTransfer", "The money order transfer is incomplete."
"eSBlockInterrupt", "You have been blocked for interrupting the server."
"eSPasswdExpired", "Your password is expired, please change your password."
"eSInvalidCreditsCard", "Invalid credit card number."
"eSBlockTrickery", "You have been blocked for trickery.  Play nice."
"eSBlockHackingRelation", "You have been blocked for suspected hacking."
"eSBlockPenaltyBox", "You are in the penalty box."
"eSInvalidClass", "eSInvalidClass"
"eSBlockRepetitiveInterrupt", "You have been blocked for repetitive interruption."
"eSBlockCreditCard", "eSBlockCreditCard"
"eSInvalidPasswd", "Invalid password."
"eSAllowedIP", "The IP address is allowed."
"eSInvalidCity", "Invalid city."
"eSInvalidFax", "Invalid fax number"
"eSPassQuizRequired", "A password quiz is required."
